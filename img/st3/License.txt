Collection name: Extended set of social icons
Author: Lenka Mel��kov�
Author homepage: http://www.profimagazin.cz
License: Creative Commons Attribution-No Derivative Works 3.0
Icons are free for personal and commercial purpose. You are free to distribute them under one condition - put a back link to www.profimagazin.cz.
License URL: http://creativecommons.org/licenses/by-nd/3.0/