(function ($) {
	$(document).ready(function(){
		$("#social-bar a").rotate({ 
			bind: 
			{ 
				mouseover : function() { 
					$(this).rotate({animateTo:360})
				},
				mouseout : function() { 
					$(this).rotate({animateTo:720})
				}
			}
		});
		
		$("#social-bar a[title]").tooltip({
			position: "center right",
			offset: [0, 25],
			opacity: 0.8,
			effect: "fade",
			fadeInSpeed: 200,
			fadeOutSpeed: 0			
		});
	});
})(jQuery);