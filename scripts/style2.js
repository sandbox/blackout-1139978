(function ($) {
	$(document).ready(function(){
		$("#social-bar a").mouseover(function(){
			$(this).stop().animate({marginLeft:0},300);
		});
		$("#social-bar a").mouseout(function(){
			$(this).stop().animate({marginLeft:-15},280);
		});
		
		$("#social-bar a[title]").tooltip({
			position: "center right",
			offset: [0, 25],
			opacity: 0.8,
			effect: "fade",
			fadeInSpeed: 200,
			fadeOutSpeed: 0			
		});
	});
})(jQuery);